Feature: Testing login functionality
  It will covers happy flow and validation tests

  Scenario: Login - happy flow
    Given Registered user with valid username and password
    When Login request is sent
    Then Response has status code is 200
    And Response contains token and refreshToken