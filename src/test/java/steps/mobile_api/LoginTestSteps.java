package steps.mobile_api;

import io.cucumber.java8.En;
import rest.mobile_api.helpers.MobileApiHelpers;
import rest.mobile_api.responses.LoginResponse;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class LoginTestSteps implements En {

    private String email;
    private String password;
    private StepData stepData;

    public LoginTestSteps(StepData stepData) {
        this.stepData = stepData;

        Given("Registered user with valid username and password", () -> {
            email = stepData.username;
            password = stepData.password;
        });

        When("Login request is sent", () -> {
            stepData.response = MobileApiHelpers.sendLoginRequest(stepData.uri, email, password);
        });

        Then("Response contains token and refreshToken", () -> {
            String token = LoginResponse.getToken(stepData.response);
            String refreshToken = LoginResponse.getRefreshToken(stepData.response);

            assertThat("Token is not null ", token, is(notNullValue()));
            assertThat("RefreshToken is not null ", refreshToken, is(notNullValue()));

        });

    }

}
