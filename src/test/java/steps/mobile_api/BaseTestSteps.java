package steps.mobile_api;

import common.environment.Environment;
import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java8.En;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import org.aeonbits.owner.ConfigFactory;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BaseTestSteps implements En {

    private StepData stepData;

    public BaseTestSteps(StepData stepData) {
        this.stepData = stepData;

        Then("Response has status code is {int}", (Integer int1) -> {
            assertThat("Response has status code ", stepData.response.getStatusCode(), is(200));
        });

    }

    @Before
    public void beforeTest() {
        RestAssured.requestSpecification = new RequestSpecBuilder().build().noFilters().filter(new AllureRestAssured());
        stepData.envName = System.getProperty("env");
        ConfigFactory.setProperty("env", stepData.envName);
        Environment testEnvironment = ConfigFactory.create(Environment.class);
        stepData.uri = testEnvironment.baseUrl();
        stepData.username = testEnvironment.username();
        stepData.password = testEnvironment.password();
    }


}
