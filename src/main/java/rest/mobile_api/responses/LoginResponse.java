package rest.mobile_api.responses;

import io.restassured.response.Response;

public class LoginResponse {

    public static String getToken(Response response){
        return response.getBody().jsonPath().get("token");

    }

    public static String getRefreshToken(Response response){
        return response.getBody().jsonPath().get("refreshToken");

    }
}
