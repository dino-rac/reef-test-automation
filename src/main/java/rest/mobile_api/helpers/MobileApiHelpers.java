package rest.mobile_api.helpers;

import com.google.gson.Gson;


import common.rest_helpers.HelperRest;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rest.mobile_api.requests.LoginRequest;
import static org.hamcrest.Matchers.nullValue;

public class MobileApiHelpers {

    public static Logger logger = LoggerFactory.getLogger(MobileApiHelpers.class);
    private static Gson gson;

    public static Response sendLoginRequest(String uri, String email, String password) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        gson = new Gson();

        LoginRequest loginRequestBody = new LoginRequest();
        loginRequestBody.setEmail(email);
        loginRequestBody.setPassword(password);

        String jsonBody = gson.toJson(loginRequestBody);

        builder.setBaseUri(uri);
        builder.setBasePath("/users/login");
        builder.setContentType("application/json");
        builder.setBody(jsonBody);
        RequestSpecification rspec = builder.build();

        Response response = HelperRest.sendPostRequest(rspec);


        if(response.toString().equals(nullValue()) || response.toString().contains("error"))
        {
            logger.error("Send login request has error: " + response.toString());
        }
        return response;

    }

}
