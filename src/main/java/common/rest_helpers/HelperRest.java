package common.rest_helpers;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.given;

/**
 * Class provide helpers for rest testing such as send get, post, put, delete requests
 *
 * @author: dino.rac
 */
public class HelperRest {

    /**
     * Generic method for sending Post request when request specification object
     * is inserted as parameter
     *
     * @param rspec request specification object
     * @return response which can be used for assertion
     * @author: dino.rac
     */
    public static Response sendPostRequest(RequestSpecification rspec) {

        return given().spec(rspec).log().all().relaxedHTTPSValidation().when()
                .post().then().log().all().extract().response();
    }

    /**
     * Generic method for sending Get request when request specification object
     * is inserted as parameter
     *
     * @param rspec request specification object
     * @return response which can be used for assertion
     * @author: dino.rac
     */
    public static Response sendGetRequest(RequestSpecification rspec) {
        return given().spec(rspec).log().all().relaxedHTTPSValidation().when()
                .get().then().log().all().extract().response();
    }

    /**
     * Generic method for sending Put request when request specification object
     * is inserted as parameter
     *
     * @param rspec request specification object
     * @return response which can be used for assertion
     * @author: dino.rac
     */
    public static Response sendPut(RequestSpecification rspec) {

        return given().spec(rspec).log().all().relaxedHTTPSValidation().when()
                .put().then().log().all().extract().response();

    }

    /**
     * Generic method for sending Delete request when request specification object
     * is inserted as parameter
     *
     * @param rspec request specification object
     * @return response which can be used for assertion
     * @author: dino.rac
     */

    public static Response sendDeleteRequest(RequestSpecification rspec) {
        return given().spec(rspec).log().all().relaxedHTTPSValidation().when()
                .delete().then().log().all().extract().response();

    }

}


