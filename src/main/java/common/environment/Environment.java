package common.environment;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:${env}.properties"})
public interface Environment extends Config {

    String baseUrl();

    String username();

    String password();




}
